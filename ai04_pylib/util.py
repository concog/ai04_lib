import numpy as np
import scipy.signal as signal

def normalise(X, detrend=False):
    '''
    Linear-detrend (optional), subtract mean, divide by std
    '''
    if detrend:
        X = signal.detrend(X, axis=0)

    devs = np.std(X, axis=0)
    if np.any(devs == 0):
        print("[util::normalise] Warning: observed a standard deviation value of "
              "zero while normalising (i.e. a constant signal), using a value of one instead to avoid division by zero.")
        idxs = np.where(devs == 0)
        devs[idxs[0]] = 1

    return (X - np.mean(X, axis=0)) / devs
