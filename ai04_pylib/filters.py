import scipy.io
from scipy import signal
from scipy.signal import butter
import numpy as np

# Calculates power in given frequency bands (equivalent to Matlab's bandpower function)
# Returns a dictionary with frequency ranges as keys and powers as values
def bandpower(x, fs, freqs, keys=None, return_periodogram=False, log_normalised=False, nperseg_factor=1):

    # Window size
    nperseg = round(x.shape[0]/nperseg_factor)

    # Estimate PSD with Welch's method
    f, Pxx = signal.welch(x, fs=fs, nperseg=nperseg, axis=0)

    rows, cols = freqs.shape

    powers = {}
    width = np.append(np.diff(f), 0)

    # Loop through the defined frequency bands and compute power, normalize by total power
    total_power = np.dot(width, Pxx)
    for row in range(rows):
        fmin = freqs[row, 0]
        fmax = freqs[row, 1]
        ind_min = scipy.argmax(f > fmin) - 1
        ind_max = scipy.argmax(f > fmax)
        power = np.dot(width[ind_min:ind_max+1], Pxx[ind_min:ind_max+1])

        if keys is not None:
            key = keys[row]
        else:
            key = str(fmin) + "-" + str(fmax) + "Hz"

        if log_normalised:
            powers[key] = np.log(np.median(power)+1)/np.log(np.median(total_power)+1)
        else:
            powers[key] = power/total_power

    if return_periodogram:
        return powers, Pxx, f
    else:
        return powers


# filter_type is one of: 'low', 'high', 'bandpass'
def butter_filter(filter_type, data, cutoff, fs, order=5):
    # Nyquist frequency
    nyq = 0.5*fs

    # Cut-off frequencies
    normal_cutoff = cutoff/nyq

    # Butterworth parameters
    b, a = butter(order, normal_cutoff, btype=filter_type, analog=False)

    # Filter the signal
    y = signal.filtfilt(b, a, data, axis=0)

    return y

def moving_average_smooth(data, window_size):
    out0 = np.convolve(data, np.ones(window_size, dtype=int), 'valid') / window_size
    r = np.arange(1, window_size - 1, 2)
    start = np.cumsum(data[:window_size - 1])[::2] / r
    stop = (np.cumsum(data[:-window_size:-1])[::2] / r)[::-1]
    return np.concatenate((start, out0, stop))


def laplace_smooth(data, window_size, p=0.5, sig=None):
    offset = window_size//2

    if sig is None:
        sig = offset

    laplace_window = signal.general_gaussian(window_size, p=p, sig=sig)
    smoothed = signal.fftconvolve(laplace_window, data)
    smoothed = (np.mean(data) / np.mean(smoothed)) * smoothed
    # smoothed = np.roll(smoothed, -offset)
    return smoothed[offset:-offset]