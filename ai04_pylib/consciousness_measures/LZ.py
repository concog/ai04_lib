from scipy.signal import hilbert
from scipy.stats import zscore
import numpy as np

'''
Lempel-Ziv Complexity and Lempel-Ziv Sum
'''


'''
Lempel-Ziv-Welch compression of binary input string, e.g. string='0010101'. It outputs the size of the dictionary of binary words.
'''
def cpr(string):
    # Initialise empty set
    d = set()
    w = ''

    # Go through string and add symbols to set when needed
    for c in string:
        wc = w + c
        if wc in d:
            w = wc
        else:
            d.add(wc)
            w = c

    # Return number of symbols in set
    return len(d)


'''
Input: Continuous multidimensional time series
Output: One string being the binarized input matrix concatenated comlumn-by-column
'''
def str_col(X):
    # If X is a vector we need to reshape it into an array
    if (X.ndim == 1):
        X = np.reshape(X, (len(X), 1))
    rows, cols = X.shape

    # Take the magnitude and mean of Hilbert transform
    M = abs(hilbert(X, axis=0))
    TH = np.mean(M, axis=0)

    # Transform array into binary string
    s = ''
    for i in range(rows):
        for j in range(cols):
            if M[i, j] > TH[j]:
                s += '1'
            else:
                s += '0'

    return s


def str_col_both(X):
    # If X is a vector we need to reshape it into an array
    if (X.ndim == 1):
        X = np.reshape(X, (len(X), 1))
    rows, cols = X.shape

    # Take magnitude of Hilbert transform and normalise it to make binarisation simpler
    z = zscore(abs(hilbert(X, axis=0)), axis=0)

    # Collect separate binary strings for calculating LZsum
    separate = {i: "" for i in range(cols)}
    s_row_concat = ''
    ones = 0
    for i in range(rows):
        for j in range(cols):
            if z[i, j] > 0:
                s_row_concat += '1'
                separate[j] += '1'
                ones += 1
            else:
                s_row_concat += '0'
                separate[j] += '0'

    # Now do column-wise concatenated signal
    s_col_concat = ''
    for i in sorted(separate):
        s_col_concat += separate[i]


    return s_row_concat, s_col_concat, separate, ones/(rows*cols)


'''
Input: X, timeseries which has been high-pass filtered, detrended, and normalized
Compute LZc and use shuffled result as normalization
'''
def LZc(X):
    SC = str_col(X)

    M = list(SC)
    np.random.shuffle(M)
    w = "".join(M)

    return {'LZc': cpr(SC) / float(cpr(w))}


'''
Compute both LZc and LZsum
'''
def LZc_LZsum(X):
    # Binarise X
    LZc_SC_row, LZc_SC_col, LZsum_SC, ratio = str_col_both(X)

    # Compute LZc (row-wise concatenated)
    LZc_M = list(LZc_SC_row)
    np.random.shuffle(LZc_M)
    LZc_w = "".join(LZc_M)

    LZc_set_size = cpr(LZc_SC_row)
    LZc_measure_row = LZc_set_size/float(cpr(LZc_w))

    # Compute LZc (column-wise concatenated)
    LZc_M = list(LZc_SC_col)
    np.random.shuffle(LZc_M)
    LZc_w = "".join(LZc_M)

    LZc_set_size = cpr(LZc_SC_col)
    LZc_measure_col = LZc_set_size / float(cpr(LZc_w))

    # And then LZsum
    LZsum_measure = [0, 0]
    for SC in LZsum_SC:
        LZsum_M = list(LZsum_SC[SC])
        np.random.shuffle(LZsum_M)
        LZsum_w = "".join(LZsum_M)
        LZsum_measure[0] += cpr(LZsum_SC[SC])
        LZsum_measure[1] += cpr(LZsum_w)

    return {"LZc": LZc_measure_row, "LZc_col": LZc_measure_col, "LZsum": LZsum_measure[0]/LZsum_measure[1],
            "ratio": ratio, "LZc_raw": LZc_set_size, "LZsum_raw": LZsum_measure[0]}


def LZc_LZsum_phase_shuffle(X):
    '''
    Spectral-profile preserving shuffling (presumably)
    :param X: One or multi-dimensional array of data
    :return: LZc and LZsum of phase shuffled signal
    '''
    # Calculate the average of n phase-shuffled signals
    test = Ph_rand(X)
    phase_random = phase_shuffle(X)
    LZ_measures = LZc_LZsum(phase_random)

    return {"LZc_phase_shuffle": LZ_measures["LZc"], "LZsum_phase_shuffle": LZ_measures["LZsum"]}


'''
Phase randomisation: multi time series in, shuffled time series out - but it has same power spectrum
'''
def phase_shuffle(X):
    # NOTE WHEN DOING SCHREIBER_SURRO< MAYBE TRY EFFECT OF DIFFERENT PHASE DISTRIBUTIONS ON MEASURES
    # If X is a vector we need to reshape it into an array
    if X.ndim == 1:
        X = np.reshape(X, (len(X), 1))

    surrogates = np.fft.rfft(X, axis=0)
    # Get shapes
    (N, channels) = surrogates.shape

    # Generate random phases uniformly distributed in the
    # interval [0, 2*Pi]
    # TODO if multidimensional make same random phase for each channel
    phases = np.random.uniform(low=0, high=2*np.pi, size=(N, channels))

    # Add random phases uniformly distributed in the interval [0, 2*Pi]
    surrogates *= np.exp(1j * phases)

    # Calculate IFFT and take the real part, the remaining imaginary part
    # is due to numerical errors.
    return np.real(np.fft.irfft(surrogates, n=X.shape[0], axis=0))


def Ph_rand(original_data):
    '''
    phase randomisation: multi time series in, shuffled time series out - but it has same power spectrum
    '''
    # NOTE WHEN DOING SCHREIBER_SURRO< MAYBE TRY EFFECT OF DIFFERENT PHASE DISTRIBUTIONS ON MEASURES

    surrogates = np.fft.rfft(original_data, axis=1)
    #  Get shapes
    (N, n_time) = original_data.shape
    len_phase = surrogates.shape[1]

    #  Generate random phases uniformly distributed in the
    #  interval [0, 2*Pi]
    phases = np.random.uniform(low=0, high=2 * np.pi, size=(N, len_phase))

    #  Add random phases uniformly distributed in the interval [0, 2*Pi]
    surrogates *= np.exp(1j * phases)

    #  Calculate IFFT and take the real part, the remaining imaginary part
    #  is due to numerical errors.
    return np.ascontiguousarray(np.real(np.fft.irfft(surrogates, n=n_time, axis=1)))


def test_normalisation(X):
    SC = str_col(X)

    M = list(SC)
    np.random.shuffle(M)
    w = "".join(M)

    a = list(map(int, SC))
    p = sum(a)/len(a)

    return [p, cpr(SC), cpr(w)]
    #return cpr(SC)
