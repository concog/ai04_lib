import numpy as np
from scipy.signal import hilbert

# NOTE! These channels are zero-indexed (index of first channel is zero)
MEG_channels = {"odd": np.array([25, 39, 65, 79])-1, "even": np.array([26, 40, 66, 80])-1,
                "distributed_odd": np.array([13, 49, 51, 59, 99, 159])-1,
                "distributed_even": np.array([14, 50, 52, 60, 100, 160])-1}

# In the paper time lag was chosen to be ~23ms => with 250Hz sampling frequency that's 5.75 ~ 6 samples
# Embedding dimension was 3. Channels contain names of channels
def PLE(X, dim=3, lag=6, channels=None):
    '''
    :param X: Band-pass filtered data
    :param dim: Embedding dimension
    :param lag: Time lag
    :param channels: Channel names
    :return: Phase-lag entropy
    '''
    # Number of channels
    nchannels = X.shape[1]
    # Number of patterns
    u_len = 2**dim
    edges1 = np.arange(u_len+1)

    # Analytic signal of X
    a_sig = hilbert(X, axis=0)

    # How many channel pairs we have?
    npairs = int(nchannels*(nchannels-1)/2)
    P = np.zeros((u_len, npairs))

    if channels is None:
        ple = np.zeros(npairs)
    else:
        ple = dict()

    # Channel pair index
    cpair = 0
    for c1 in range(nchannels-1):
        for c2 in range(c1+1, nchannels):
            # 1. Calculate sign of phase difference
            c_sig = a_sig[:, c1] * np.conj(a_sig[:, c2])
            c_sign = np.sign(np.imag(c_sig))

            # 2. Delay reconstruction
            # Phase reconstruction using time delay embedding
            Ddata = delay_recons(c_sign, lag, dim)
            # Binary representation
            Ddata_symbol = heaviside(Ddata)

            # 3. Binary -> decimal
            udata = np.zeros(Ddata_symbol.shape[0])
            for i in range(dim):
                udata += Ddata_symbol[:, i] * 2**i

            # 4. Calculate probability and entropy
            p = np.histogram(udata, edges1)[0]
            p = p/len(udata)

            # P[:, cpair] = p

            ple_tmp = -np.dot(p, (np.log2(p+np.finfo(float).eps)))

            if channels is None:
                ple[cpair] = ple_tmp / dim
                cpair += 1
            else:
                pair_name = "c" + str(channels[c1]) + "_" + str(channels[c2])
                ple[pair_name] = ple_tmp / dim

    return ple


'''
Input:
  X, 2-d matrix (time by channel)
  lag, delay time
  dim, embedding dimension
'''
def delay_recons(X, lag, dim):
    if X.ndim == 1:
        X = np.reshape(X, (len(X), 1))
    (N, nchannel) = X.shape

    # y = np.zeros((N - lag*(dim-1), dim, nchannel))
    y = np.zeros((N - lag * (dim - 1), dim))
    for c in range(nchannel):
        for j in range(dim):
            y[:, j] = X[j*lag:N-(dim-(j+1))*lag].squeeze()

    return y


def heaviside(x):
    return 0.5*(np.sign(x) + 1)