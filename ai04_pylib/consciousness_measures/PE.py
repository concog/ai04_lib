from pyentrp import entropy


def PE(X, dim=3, lag=1):
    '''
    Calculates permutation entropy
    :param X: Signal
    :param dim: Embedding dimension
    :param lag: Lag
    :return: Permutation entropy
    '''
    return entropy.permutation_entropy(X, dim, lag)
