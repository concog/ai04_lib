import numpy as np
import matplotlib.pyplot as plt

# Calculate Zero Set Fractal Dimension (https://www.fose1.plymouth.ac.uk/spmc/staff/ghenderson/GEOFF-TMBE-01658150.pdf)
def ZSFD(data, fs):
    # Repeat for t = [30, 60, 90, ..., min(500, len(signal)] ms
    nsamples = np.shape(data)[0]
    limit_seconds = min(0.5, nsamples*(1/fs))
    dt = np.linspace(0.03, limit_seconds, 20)
    ds = np.floor(fs*dt)

    # Calculate zero crossing indices
    idxs = zero_crossings(data)

    #plt.plot(data)
    #idxs = np.asarray(idxs)
    #plt.plot([idxs, idxs + 1], [data[idxs], data[idxs + 1]], 'r')
    #plt.plot([0, len(data)], [0, 0], 'k--')

    # Go through t and calculate total lengths
    L = []
    for s in ds:
        count = fit_lines(idxs, s)
        L.append(count*s)

    # Fit the equation
    if sum(L) == 0:
        return False
        p = [0]
    else:
        p = np.polyfit(np.log(ds), np.log(L), 1)

    return 1-p[0]


def fit_lines(idxs, t):
    if len(idxs) == 0:
        return 0

    count = 0
    scope = 0
    current = 0
    while scope <= idxs[-1]:
        scope = idxs[current] + t
        count += 1
        while current < len(idxs) and idxs[current] < scope:
            current += 1

    return count

def zero_crossings(data):
    idxs = []

    previous = 0
    begin_sample = 0
    while previous == 0:
        previous = np.sign(data[begin_sample])
        begin_sample += 1

    for sample in range(begin_sample, len(data)):
        current = np.sign(data[sample])
        if current == 0:
            continue
        elif current != previous:
            idxs.append(sample-1)
            previous = current

    return idxs
