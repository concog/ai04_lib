import numpy as np
from scipy.signal import welch


def SSE(data, fs, window_len, cutoff_freq):
    f, pxx = welch(data.T, fs, nperseg=window_len)

    # Should we remove 1/f power?
    #sub = remove_f_power(f, pxx, 40)
    pxx = np.mean(pxx, axis=0)

    # Ignore low and high frequencies (in case signal has been filtered)
    idx_low = find_nearest_idx(f, cutoff_freq[0])
    idx_high = find_nearest_idx(f, cutoff_freq[1])
    pxx = pxx[idx_low:idx_high]

    # Normalise spectrum to make it a probability distribution
    pxx_norm = pxx/sum(pxx)

    sse = -sum(pxx_norm*np.log2(pxx_norm))

    return sse


def find_nearest_idx(array, value):
    idx = (np.abs(array-value)).argmin()
    return idx


def remove_f_power(f, pxx, cutoff=None):
    if cutoff is None:
        cutoff = f[-1]

    # Find idx corresponding to cutoff frequency
    idx = find_nearest_idx(f, cutoff)

    # Fit a line
    p = np.polyfit(f[:idx], np.log(pxx[:idx]), 1)

    # Create 1/f distribution
    dist = np.exp(f[:idx]*p[0] + p[1])

    # Remove from original distribution
    sub = pxx[:idx] - dist

    return sub