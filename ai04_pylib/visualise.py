import numpy as np
import matplotlib.pyplot as plt
import scipy.signal


def plot_frequency_spectrum(data, fs):
    f, Pxx_den = scipy.signal.welch(data, fs, nperseg=len(data), axis=0)
    #  plt.semilogy(f, Pxx_den)
    plt.plot(f, Pxx_den)
    plt.xlabel('frequency [Hz]')
    plt.ylabel('PSD [V**2/Hz]')
    plt.show()