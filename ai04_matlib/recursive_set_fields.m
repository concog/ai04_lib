function s = recursive_set_fields(s, fields, idx1, idx2, x)
if numel(fields) > 1
    sub = recursive_set_fields(s.(fields{1}), fields(2:end), idx1, idx2, x);
    s.(fields{1}) = sub;
else
    s.(fields{1})(idx1,idx2) = x;
end
end