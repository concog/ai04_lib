function s = recursive_set(s, fields, x)
if numel(fields) > 1
    if ~isfield(s, fields{1})
        s.(fields{1}) = struct();
    end
    sub = recursive_set(s.(fields{1}), fields(2:end), x);
    s.(fields{1}) = sub;
else
    s.(fields{1}) = x;
end
end