function p = ranksum_array(X, Y, varargin)

if size(X,2) ~= size(Y,2)
    error('X and Y must have equal number of columns');
end

p = nan(1, size(X, 2));

for k = 1:size(X, 2)
    if all(isnan(X(:,k))) || all(isnan(Y(:,k)))
        continue
    end
    p(k) = ranksum(X(:,k), Y(:,k), varargin{:});
end

end