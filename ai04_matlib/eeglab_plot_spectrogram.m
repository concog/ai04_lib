function [s_tot, w, t] = eeglab_plot_spectrogram(EEG, varargin)
% EEGLAB_PLOT_SPECTROGRAM Plot spectrogram of given eeglab data structure
%   
%   [s_tot, w, t] = EEGLAB_PLOT_SPECTROGRAM(EEG) Plot spectrogram of eeglab 
%   data structure with default window size (4 seconds). Note that epochs 
%   are ignored if EEG is epoched. s_tot contains channel-wise power values 
%   for each time point (t) and frequency (w).
%
%   [s_tot, w, t] = EEGLAB_PLOT_SPECTROGRAM(EEG, 'channels', {'E70', 'E75',
%   'E83'}, 'window_size', 2, 'threshold', 75) uses optional arguments
%      - channels: a list of integers or labels
%      - window_size: size of window in seconds (time resolution)
%      - threshold: cutoff threshold for low values (a percentage between 0
%      and 100). Power differences are sometimes easier to see when lower
%      values are ignored. A percentage of 75 typically works well.
%
%   Note that this function doesn't use any eeglab functionality although
%   the input parameter is assumed to be an eeglab data structure. It's
%   straightforward to modify this function to work with other data
%   formats.
%
%   Also, you should filter the data and remove bad channels before using
%   this function.
%
%   2019/07/24 Aleksi Ikkala

% Define default parameters here
params = struct('channels', 1:EEG.nbchan, 'window_size', 4, 'threshold', 0);

% Parse given arguments
params = parse_varargin(params, varargin{:});

% Transform channel labels into indices if necessary
if iscell(params.channels) || isstring(params.channels)
    labels = {EEG.chanlocs.labels};
    params.channels = find(ismember(labels, params.channels));
end

% Need to reshape epoched data here
if EEG.trials > 1
    data = reshape(EEG.data, EEG.nbchan, EEG.pnts*EEG.trials)';
else
    data = EEG.data';
end

% Calculate spectrogram separately for each channel
s_tot = cell(numel(params.channels),1);
for idx = 1:numel(params.channels)
    channel_idx = params.channels(idx);
    [s,w,t] = spectrogram(data(:,channel_idx), EEG.srate*params.window_size, 0, [], EEG.srate);
    s_tot{idx} = log(s.*conj(s));
end

% Calculate median PSD for each freq/timepoint
s_tot = cat(3, s_tot{:});
s_channel_avg = nanmedian(s_tot,3);

% Cutoff low values to make resolution better
if params.threshold
    threshold = prctile(s_channel_avg(:), params.threshold);
    zero_idxs = s_channel_avg < threshold;
    s_channel_avg(zero_idxs) = nan;
end

% Plot the spectrogram and flip y-axis
t = t/60;
figure; imagesc(t,w,s_channel_avg);
ax = gca; ax.YDir = 'normal';

% Set axis limits and titles etc
title(EEG.filename, 'interpreter', 'none'); ylabel('Frequency (Hz)'); xlabel('Time (mins)');

end


function params = parse_varargin(params, varargin)
% PARSE_VARARGIN Parse varargin given to a function. First input parameter
% contains default params or is an empty struct

% Varargin must have an even number of elements (each key must have a
% value)
pairs = numel(varargin)/2;

if mod(pairs, 1) ~= 0
    error('Each key must have a value');
end

% Get keys and values
for pair_idx = 1:2:2*pairs
    key = char(varargin{pair_idx});
    value = varargin{pair_idx+1};
    
    params.(key) = value;
end

end