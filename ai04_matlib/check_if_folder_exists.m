function check_if_folder_exists(path, delete_files)

if nargin < 2
    delete_files = false;
end

% Check if this folder exists
create_folder = false;
if isdir(path)
    % Check if there are files in this folder
    processed_files = dir(path);
    if length(processed_files) > 2
        if ~delete_files
            % Ask if user wants to delete files
            input_msg = sprintf('There are processed files in folder\n\n%s\n\ndelete them? (yes/no)\n', path);
            in = '';
            while ~strcmp(in, 'no') && ~strcmp(in, 'yes') 
                in = input(input_msg, 's');
            end
        end
        % Delete the files if the user wants that
        if delete_files || strcmp(in, 'yes')
            % Make sure there are no open files
            fclose('all');
            % Try to remove the folder
            if ~rmdir(path, 's')
                error_msg = ['Couldn''t remove folder ' path ', aborting'];
                error(error_msg);
            end
            create_folder = true;
        end
    end
else
    create_folder = true;
end
   
% Create the folder if it doesn't exist or if it was removed
if create_folder
    if mkdir(path)
        fprintf('Created folder\n\n%s\n', path);
    else
        error_msg = ['Couldn''t create folder\n\n' path '\n'];
        error(error_msg);
    end
end
end