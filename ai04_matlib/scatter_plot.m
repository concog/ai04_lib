function fig = scatter_plot(x, y, varargin)

% Define default parameters and parse varargin
params = struct('correlation_type', 'pearson', 'remove_outliers', false, 'xlabel', [], 'ylabel', []);
params = parse_varargin(params, varargin{:});

% Remove outliers if needed
if params.remove_outliers
    outliers = isoutlier(x,'quartiles') | isoutlier(y,'quartiles');
    x(outliers) = [];
    y(outliers) = [];
end

% Plot
fig = figure(); hold on;
scatter(x, y, 100, '.');

% Calculate correlation coefficients
[r, p] = corr(x, y, 'type', params.correlation_type, 'rows', 'pairwise');

% Set labels and title
xlabel(params.xlabel);
ylabel(params.ylabel);
title_msg = sprintf('%s r = %.2f, p = %.3d, N = %d', params.correlation_type, r, p, sum(~isnan(x) & ~isnan(y)));

if params.remove_outliers
    title_msg = sprintf('%s\noutliers removed',title_msg);
end

title(title_msg);

end