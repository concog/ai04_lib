function legs = get_legend(handle)
% Returns legends of a figure whose handle is given
legs = findobj(handle, 'Type', 'Legend');
if isempty(legs)
    legs = "";
else
    legs = string(legs.String);
end
end