function start_cbupool(num_workers, mem_per_worker)
%START_CBUPOOL Starts a pool of workers in the new CBU cluster

% Add this path for cbupool command
addpath /hpc-software/matlab/cbu/

% Close previous pool
existing_pool = gcp('nocreate');
if (~isempty(existing_pool))
    delete(existing_pool);
end

% TODO: Remove old cluster profiles -- is there a way to do this programmatically?

% Get cluster object
WallTime = 48;
P = cbupool(num_workers);
P.SubmitArguments = sprintf('--ntasks=%d --mem-per-cpu=%dG --time=%d:00:00', num_workers, mem_per_worker, WallTime);  
P.JobStorageLocation = '/home/ai04/Workspace/matlab_parallel_jobs/';

% Open the parallel pool
parpool(P, num_workers);

end
