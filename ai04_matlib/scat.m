function h = scat(X, features, varargin)

if iscell(X)
    varnames = X{2};
    X = X{1};
else
    varnames = [];
end

if ischar(varargin{end}) || isstring(varargin{end})
    h = get_figure(strjoin(varargin{end},''), 1);
    argin = numel(varargin)-1;
else
    h = figure;
    argin = numel(varargin);
end

hold on;
if numel(features) == 1
    for group = 1:argin
        histogram(X(varargin{group},features),20);
    end
elseif numel(features) == 2
    for group = 1:argin
        scatter(X(varargin{group},features(1)), X(varargin{group},features(2)), '.');
    end
elseif numel(features) == 3
    for group = 1:argin
        scatter3(X(varargin{group},features(1)), X(varargin{group},features(2)), X(varargin{group},features(3)), 'o');
    end
else
    error('Can plot only max 3 dimensions');
end

if ~isempty(varnames)
    msg = cell(1,numel(features));
    for idx = 1:numel(features)
        msg{idx} = [varnames{features(idx)} ' (feature #' num2str(features(idx)) ')'];
    end
else
    if size(features,1)==1
        features = features';
    end
    msg = ['Features: ' num2str(features')];
end

title(msg, 'Interpreter','none');

end

