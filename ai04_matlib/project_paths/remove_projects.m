function remove_projects(projects)
% REMOVE_PROJECTS Remove projects from path

% Set warnings off; otherwise there'll be a lot of warnings about removing
% folders from path that weren't in path in the first place
warning off

for idx = 1:numel(projects)
    fprintf('Removing project [%s] from path\n', projects(idx).name);
    rmpath( genpath(fullfile(projects(idx).path, 'scripts')) );
end

warning on

end