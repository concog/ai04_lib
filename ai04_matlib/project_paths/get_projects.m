function projects = get_projects(names)
% GET_PROJECTS Get projects' names and paths

root = '/home/ai04/Workspace/projects/';
projects_dir = dir(root);

% Remove first two entries and files
projects_dir(1:2) = [];
projects_dir(~[projects_dir.isdir]) = [];

projects = struct();
idx_project = 1;
for i = 1:numel(projects_dir)
    
    % Don't add hidden folders
    if projects_dir(i).name(1) == "."
        continue;
    end
    
    % Don't add symbolic links (in case a project has been renamed, e.g.
    % "power_correlations" was renamed to "camcan," but a symlink
    % "power_correlations" was retained in case someone was using code in
    % that project)
    project_path = fullfile(projects_dir(i).folder, projects_dir(i).name);
    if unix(sprintf('test -L %s', project_path)) == 0
        continue;
    end
    
    projects(idx_project).name = projects_dir(i).name;
    projects(idx_project).path = project_path;
    idx_project = idx_project+1;
    
end

if nargin > 0
    % Return only those projects that were queried
    rm_idxs = string({projects.name}) ~= names;
    projects(rm_idxs) = [];           
end
end