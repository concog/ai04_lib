function add_projects(projects)
% ADD_PROJECTS Add projects to path

% Set warnings off, matlab complains about CBU's "copyfile" wrapper
warning off

for project_idx = 1:numel(projects)
    project = projects(project_idx);

    % Find newest version of scripts
    latest = -1;
    latest_f = "";
    files = dir(fullfile(project.path, 'scripts'));
    for file_idx = 3:numel(files)
        filename = files(file_idx).name;
        if filename(1) == "v"
            try
                num = str2double(filename(2:end));
                if num > latest
                    latest = num;
                    latest_f = filename;
                end
            catch 
            end
        end
    end
    
    if latest == -1
        scripts_path = fullfile(project.path, 'scripts');
    else
        scripts_path = fullfile(project.path, 'scripts', latest_f);
    end
    
    fprintf('Adding project [%s] to path (%s)\n', project.name, scripts_path);
    addpath(genpath(scripts_path));

end
    
warning on
end