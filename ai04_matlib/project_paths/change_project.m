function change_project()

% TODO: separate functions add_project and remove_project, use them

% Get all projects
projects = get_projects();

% Ask which project user wants
indx = listdlg('ListString', {projects.name}, 'listsize', [260, 280], 'name', 'Choose a project', ...
    'selectionmode', 'multiple');
chosen_project = projects(indx);

% Remove paths of other projects
projects(indx) = [];

% Remove all other projects
remove_projects(projects);

% Add the chosen project
add_projects(chosen_project)

end

