function p = signrank_array(dfrnc, m, varargin)

p = nan(1, size(dfrnc,2));

if nargin < 2
    m = 0;
end

parfor k = 1:size(dfrnc, 2)
    if sum(isnan(dfrnc(:,k))) == size(dfrnc,1)
        continue;
    end
    p(k) = signrank(dfrnc(:,k), m, 'method', 'exact', varargin{:});
end

end