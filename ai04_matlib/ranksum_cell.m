function [p, h, z] = ranksum_cell(X, varargin)

% Make sure X is in correct format
if ~isvector(X) || ~iscell(X) || any(~cellfun(@isvector, X))
    error('X should be a cell vector containing vectors');
end

% Collect p-values and z-statistics into a symmetric square matrix
p = nan(numel(X), numel(X));
z = nan(numel(X), numel(X));

% Go through all pairs
for i = 1:numel(X)
    for j = i+1:numel(X)
        if all(isnan(X{i})) || all(isnan(X{j}))
            continue
        end
        [p(i,j), ~, stats] = ranksum(X{i}, X{j}, varargin{:});
        z(i,j) = stats.zval;
    end
end

% Let's also return BH-FDR significant differences; use only upper
% triangle so that we won't increase number of "comparisons"
h_vec = fdr_bh(p(logical(triu(ones(size(p)),1))));

% Transform h_vec into a square matrix
h = zeros(size(p));
h(logical(triu(ones(size(h)),1))) = h_vec;

% Make p, h, and z symmetric
p(logical(tril(ones(size(p)),-1))) = 0;
z(logical(tril(ones(size(z)),-1))) = 0;
p = p+p';
z = z+z';
h = h+h';

% Fill diagonal of p with ones and diagonal of z with zeros
p(logical(eye(size(p)))) = 1;
z(logical(eye(size(z)))) = 0;

end