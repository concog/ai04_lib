function vec = scale_vector(vec, minval, maxval)
% Scale to [0,1] first
vec = vec - min(vec);
vec = vec./max(vec);

% Then scale to [minval,maxval]
vec = vec.*(maxval - minval);
vec = vec + minval;
end