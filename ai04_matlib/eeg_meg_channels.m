function found_labels = eeg_meg_channels()

meg_file = '/home/ai04/Downloads/neuromag306planar.mat';
eeg_file = '/home/ai04/Downloads/acticap-64ch-standard2.mat';

% Load coordinates
meg = load(meg_file);
meg = meg.neuromag306planar;
eeg = load(eeg_file);
eeg = eeg.lay;

meg_x = cell2mat(meg(:,2));
meg_x = reshape(meg_x, 2, 102);
meg_x = mean(meg_x)';
meg_x = scale_vector(meg_x, min(eeg.pos(:,1)), max(eeg.pos(:,1)));

% meg_x(meg_x>0) = meg_x(meg_x>0)./max(meg_x(meg_x>0));
% meg_x(meg_x>0) = meg_x(meg_x>0).*max(eeg.pos(:,1));
% meg_x(meg_x<0) = meg_x(meg_x<0)./max(abs(meg_x(meg_x<0)));
% meg_x(meg_x<0) = meg_x(meg_x<0).*abs(min(eeg.pos(:,1)));

meg_y = cell2mat(meg(:,3));
meg_y = reshape(meg_y, 2, 102);
meg_y = mean(meg_y)';
meg_y = scale_vector(meg_y, min(eeg.pos(:,2)), max(eeg.pos(:,2)));

% meg_y(meg_y>0) = meg_y(meg_y>0)./max(meg_y(meg_y>0));
% meg_y(meg_y>0) = meg_y(meg_y>0).*max(eeg.pos(:,2));
% meg_y(meg_y<0) = meg_y(meg_y<0)./max(abs(meg_y(meg_y<0)));
% meg_y(meg_y<0) = meg_y(meg_y<0).*abs(min(eeg.pos(:,2)));

meg_coords = [meg_x, meg_y];

get_figure(mfilename, 1), plot(eeg.pos(:,1), eeg.pos(:,2),'x','linewidth',2)
hold on, plot(meg_x,meg_y,'x','linewidth',2)

all_labels = string(reshape({meg{:,6}}',2,102));
meg_labels = strings(102,1);
for idx = 1:size(all_labels,2)
    meg_labels(idx) = strjoin(all_labels(:,idx),' / ');
end


% EEG cap labels we want to find
eeg_labels = [
    "Oz", "O1", "O2", ...
    "C3", "C4", ...
    "PO10", ...
    "T7", "T8", "TP8", "FT10", "TP10", ...
    "F7", "F8", "Fz"
    ];

% Go through the labels and find corresponding labels for meg
for idx = 1:numel(eeg_labels)
    label = eeg_labels{idx};
    eeg_idx = find(eeg.label==string(label));
    
    % Find this label's coordinates
    coords = eeg.pos(eeg_idx, :);
    plot(coords(1),coords(2),'bo','linewidth',2);
    
    % Find MEG sensors closest to this location
    dists = zeros(size(meg_coords,1),1);
    for row = 1:size(meg_coords,1)
         distance = pdist([coords; meg_coords(row,:)]);
         dists(row) = distance;
    end
    
    % Shortest distance
    meg_idxs = find(dists==min(dists));
    plot(meg_coords(meg_idxs,1), meg_coords(meg_idxs,2),'ro','linewidth',2);

    found_labels.(label) = {meg_labels(meg_idxs), [(meg_idxs*2)-1, meg_idxs*2]};
    
end

end


