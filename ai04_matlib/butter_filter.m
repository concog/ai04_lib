function filtered_signal = butter_filter( signal, fs, type, cutoff, order )
%BUTTER_FILTER Filter a signal
%   FILTERED_SIGNAL = BUTTER_FILTER(SIGNAL, FS, TYPE, CUTOFF) filters the
%   SIGNAL with a TYPE filter. TYPE is 'low', 'high', or 'bandpass'. 
%   CUTOFF determines cutoff frequencies (1x1 double array for
%   'lowpass' or 'highpass' filters, 1x2 double array for 'bandpass'). FS 
%   is sampling frequency. SIGNAL can be a vector or a matrix; if SIGNAL is
%   a matrix, columns will be treated as signals.

% filter_type = [type, 'iir'];

% Design the filter
% if strcmp(type, 'bandpass')
%     d = designfilt(filter_type, 'FilterOrder', 20, ...
%         'HalfPowerFrequency1', cutoff(1), 'HalfPowerFrequency2', cutoff(2), ...
%         'SampleRate', fs, 'DesignMethod','butter');
% else
%     d = designfilt(filter_type, 'FilterOrder', 8, ...
%         'PassbandFrequency', cutoff(1), 'PassbandRipple', 1, ...
%         'SampleRate', fs);
% end

% Default order is 5
if nargin < 5
    order = 5;
end

% Nyquist frequency
nyq = fs/2;

% Butterworth filter
[b, a] = butter(order, cutoff./nyq, type);

% Filter the signal
filtered_signal = filtfilt(b, a, signal);

end

