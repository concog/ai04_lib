function dfrnc = difference(array, varargin)
% DIFFERENCE First argument should contain data in format nfeatures x
% ntimepoint, with timepoints in increasing chronological order

defaultIndices = logical(1:size(array,1))';
defaultType = 'relative_change';
expectedTypes = {'relative_change', 'absolute_change', 'magnitude_change', 'absolute'};
defaultTimepoints = true(1,size(array,2));

p = inputParser;
addRequired(p, 'array', @(x) isnumeric(x));
addOptional(p, 'indices', defaultIndices, @(x) islogical(x) && all(size(x) == size(defaultIndices)));
addOptional(p, 'type', defaultType, @(x) ischar(x) && any(validatestring(x, expectedTypes)));
addOptional(p, 'timepoints', defaultTimepoints, @(x) islogical(x) && isvector(x));
parse(p, array, varargin{:});

idx = p.Results.indices;
type = p.Results.type;

% If there's only one timepoint, return absolute values
if sum(p.Results.timepoints)==1 && ~strcmp(p.Results.type, 'absolute')
    warning('Only one timepoint queried, changing ''type'' to ''absolute''');
    type = 'absolute';
end

% Return raw values if there's only one column in array
if size(array,2) == 1
    dfrnc = array(idx, :);
    return
end

timepoints = p.Results.timepoints;
array = array(:, timepoints);

% Return the raw values if they're wanted
if strcmp(type, 'absolute')
    dfrnc = array(idx, :);
    return
end


if strcmp(type, 'relative_change')
%     dfrnc = (array(idx,2) ./ array(idx,1)) - 1;
    dfrnc = diff(array(idx,:),1,2)./array(idx,1:end-1);
elseif strcmp(type, 'magnitude_change')
    dfrnc = abs(diff(array(idx,:),1,2)) ./ array(idx,1:end-1);    
elseif strcmp(type, 'absolute_change')
    dfrnc = diff(array(idx,:),1,2);
else
  error('Type %s not defined\n', type);
end

end