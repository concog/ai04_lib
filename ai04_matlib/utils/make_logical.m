function logical_vector = make_logical(numeric_vector, vector_length)

if min(numeric_vector) < 1 || max(numeric_vector) > vector_length || any(mod(numeric_vector, 1))
    error('Given vector must contain integers between 1 and %d', vector_length);
end

logical_vector = false(vector_length, 1);
logical_vector(numeric_vector) = true;

end