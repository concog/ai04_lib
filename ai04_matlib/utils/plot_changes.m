function fig = plot_changes(x, y, xname, yname)
% PLOT_CHANGES Produces a scatter plot of changes in x vs changes in y

fig = figure(); hold on;

% Do the scatter plot
scatter(x, y, 100, '.');
xlabel(xname); ylabel(yname);

% Plot also line to center (median) of mass
%line([0, nanmedian(x(x>0))], [0, nanmedian(y(y>0))], 'color','g', 'linewidth', 2)
%line([0, nanmedian(x(x>0))], [0, nanmedian(y(y<0))], 'color','g', 'linewidth', 2)
%line([0, nanmedian(x(x<0))], [0, nanmedian(y(y>0))], 'color','g', 'linewidth', 2)
%line([0, nanmedian(x(x<0))], [0, nanmedian(y(y<0))], 'color','g', 'linewidth', 2)
%line([0, sum(x)], [0, sum(y)], 'color', 'g', 'linewidth', 2);
line([0, nanmedian(x)], [0, nanmedian(y)], 'color','r', 'linewidth', 2)

% Set axes
ax = gca;
ax.XAxisLocation = 'origin';
ax.YAxisLocation = 'origin';

% Calculate Pearson's chi square
nans = isnan(x) | isnan(y);
[~,~,p] = crosstab(x(~nans), y(~nans));


% Set title and labels
%xlabel(sprintf('/Delta %s', xname)); ylabel(sprintf('/Delta %s', yname));
title({sprintf('Change in %s', xname), 'vs', sprintf('change in %s', yname), sprintf('Pearson''s chi square p = %.3f', p)}, ...
    'interpreter', 'none');
end