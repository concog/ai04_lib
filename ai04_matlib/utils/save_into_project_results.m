function save_into_project_results(project_name, handles, sub_folder, names, fig_type)
% SAVE_INTO_PROJECT_RESULTS Saves given figures/files into a project's
% results folder
%
%   SAVE_INTO_PROJECT_RESULTS(project_name, handles, sub_folder, names)
%   saves given 'handles' (can be a vector of figure handles, or a cell
%   vector of variables) into a project's results folder (as defined by
%   'project_name') into a subfolder structure (as defined by string array
%   'sub_folder'). Each figure/variable is saved with a name defined in
%   'names'. When saving files the first entry in 'names' defines the name
%   of the mat-file.
%
%   SAVE_INTO_PROJECT_RESULTS(project_name, handles, sub_folder, names,
%   fig_type) works only with figures; saves figures in format defined by
%   string array 'fig_type'. See 'savefigs' function for format options.

% Get this project's info
project = get_projects(project_name);

% Get date
date_time = char(datestr(now, 'yyyy-mm-dd'));

% Put together path to save folder
save_path = fullfile(project.path, 'results', sub_folder{:}, date_time);

% Make sure fig_names is a string array
names = string(names);

if iscell(handles)
    % Save variables
    
    if numel(handles) ~= numel(names)-1
        error('Incorrect number of variable names. When saving variables the first input in ''names'' must be the filename');
    end

    % Create a struct with fields as variable names
    S = struct();
    for var_idx = 1:numel(handles)
        S.(names{var_idx+1}) = handles{var_idx};
    end
    
    % Save the file
    if exist(save_path, 'dir') ~= 7
        mkdir(save_path);
    end
    save(fullfile(save_path, sprintf('%s.mat', names{1})), '-struct', 'S');

else
    % Save figures
    
    for fig_idx = 1:numel(handles)    
        if nargin > 4
            savefigs(handles(fig_idx), save_path, names(fig_idx), fig_type);
        else
            savefigs(handles(fig_idx), save_path, names(fig_idx));
        end
    end

end
end