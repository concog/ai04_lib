function params = parse_varargin(params, varargin)
% PARSE_VARARGIN Parse varargin given to a function. First input parameter
% contains default params or is an empty struct

% Varargin must have an even number of elements (each key must have a
% value)
pairs = numel(varargin)/2;

if mod(pairs, 1) ~= 0
    error('Each key must have a value');
end

for pair_idx = 1:2:2*pairs
    key = char(varargin{pair_idx});
    value = varargin{pair_idx+1};
    
    params.(key) = value;
end

end