function h = get_figure(name, varargin)
% Finds the handle of a named figure, or returns the handle of a new figure

all_figures = findobj('type', 'figure');

% Loop through these figures, is there the one we're looking for?
for idx = 1:numel(all_figures)
    if strcmp(name, all_figures(idx).Name)
        h = all_figures(idx);
        if nargin>1; clf(h); end
        % Bring figure to top
        figure(h);
        return;
    end
end

% Figure with name 'name' was not found, create it
h = figure();
h.Name = name;
figure(h);

end