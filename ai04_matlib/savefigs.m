function savefigs( handle, path, filename, types )
%SAVEFIGS Saves .fig and .png figures of given handle to given path
%   SAVEFIGS(HANDLE, PATH, FILENAME) saves the figure specified by handle
%   into given path (creates subfolders 'png' and 'fig') with the given
%   filename
%
%   SAVEFIGS(HANDLE, PATH, FILENAME, TYPE) saves only 'png' files if
%   TYPE is 'png', or 'fig' files if TYPE is 'fig'

if nargin < 4
    types = ["png", "fig"];
else
    % Make sure types is a string array and not a char vector
    types = string(types);
end

% Loop over types and save each type of figure
for type_idx = 1:numel(types)
    
    % Get figure type
    type = types{type_idx};
    
    % Create path and filename
    fullpath = fullfile(char(path), char(type));
    fullfilename = fullfile(fullpath, char(filename));
    
    % Create a folder if it doesn't exist
    create_folder(fullpath);
    
    % Save the figure
    saveas(handle, fullfilename, type);
end

% Save first a png file
%if nargin < 4 || type ~= "fig"
    %if nargin==3
%        fullpath = fullfile(path, type);
%        fullfilename = fullfile(path, type, filename);
    %else
    %    fullpath = path;
    %    fullfilename = fullfile(path, filename);
    %end
%    create_folder(fullpath);
%    saveas(handle, sprintf('%s.%s', fullfilename, type));
%end

% Then save fig 
%if nargin < 4 || type == "fig"
    %if nargin==3
%        fullpath = fullfile(path, 'fig');
%        fullfilename = fullfile(path, 'fig', filename);
    %else
    %    fullpath = path;
    %    fullfilename = fullfile(path, filename);
    %end
%    create_folder(fullpath);
%   savefig(handle, fullfilename);
%end
end

