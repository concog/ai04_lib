% Convert given input string into a title/label string (remove underscore,
% join names)
function output = title_string(input,varargin)
if ischar(input)
    input = string(input);
end


if nargin > 1
    output = strings(size(input));
    for idx = 1:numel(output)
        output(idx) = title_string(input(idx));
    end
else
    output = strjoin(input, ' / ');
    output = strrep(output, '_', ' ');
end
end