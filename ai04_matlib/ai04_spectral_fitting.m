function [fits] = SpectralFitting(F,P)

% Original code by Darren Price @ /imaging/dp01/toolboxes/alphapeaks

cv = P.initvals;

% Set up search directions
v = [1 0 0
     -1 0 0
     0 1 0
     0 -1 0
     0 0 1
     0 0 -1];
Inc = v.*repmat(P.Inc,length(v),1);

% Set up search control parameters
cflim = P.cflim;
currcf = 1e100;
bestcf = 1e100;
cfgrad = 1e100;
% cflim is a percentage of the overall sum of squares
cflim = sum(F.^2)*cflim;

% x direction for generating gaussians
xi = P.x;

% windows
[~,fitwin(1)] = closest(xi,P.FitWin(1));
[~,fitwin(2)] = closest(xi,P.FitWin(2));
fitwin = fitwin(1):fitwin(2);

% Indexing
ind = 0;
inda = 0;
iter = 1;

% F
F = F-mean(F(fitwin));

while cfgrad > cflim
    iter = iter + 1;
   
    hc = zeros(1,size(Inc,1));
    for inci = 1:size(Inc,1)
        testv = cv + Inc(inci,:);
        T = normpdf(xi,testv(1),testv(2))*testv(3);
        T = T-mean(T(fitwin));
        hc(inci) = sum((T(fitwin)-F(fitwin)).^2);
    end 
    
    dir = find(hc == min(hc),1,'first'); % find min index
    lastcf = hc(dir); % min cf from last sample
    
    % check for bad conditions
    badcond = 0;
    if testv(3) < 0
        badcond = 1;
    end    
    
    % Compare current best to overall best
    if lastcf < bestcf && badcond == 0 % is the last sample better than the current best? and check no bad conditions.
        cfgrad = abs(bestcf-lastcf); % if so calculate the gradient
        bestcf = lastcf; % set the best to the best attempt from last sample
        cv = cv + Inc(dir,:); % and update cv
        ind = 1; % reset ind (loop checking)
        inda = inda + 1;
    else
        ind = ind + 1; % count failure to improve
        inda = 1; % reset inda
    end
    
    % check if caught in loop
    if ind > 2
        Inc = Inc ./ P.decelfactor;
    end
    
    if inda > 2
        Inc = Inc .* P.accelfactor;
    end
    
%     if badcond
%         Inc = v.*repmat(P.Inc,length(v),1); % reset Inc
%     end
        
    % plot
    if P.showplots == 1;
        hchist(iter) = min(hc);
        figure(1)
        g1 = normpdf(xi,cv(1),cv(2)).*cv(3);
        T = g1;
        T = T-mean(T(fitwin));
        subplot(3,1,1)
        plot(xi,T,'r',xi,F,'b')
        subplot(3,1,2)
        plot(hchist)
        disp(str('Best = ',bestcf,' cfgrad = ',cfgrad,' iter = ',iter, ' ind = ',ind,' inda = ',inda,' Inc(1) = ',Inc(1)))
        pause(0.1)
    end
end
disp(str('Best = ',bestcf,' cfgrad = ',cfgrad,' iter = ',iter, ' ind = ',ind,' inda = ',inda,' Inc(1) = ',Inc(1)))
g1 = normpdf(xi,cv(1),cv(2));%.*cv(3);
T = g1;    
fits.T = T;
fits.cv = [cv bestcf];

