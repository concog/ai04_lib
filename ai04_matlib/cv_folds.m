function folds = cv_folds(y, nmeasurements, k)
% Create stratified folds

rng('shuffle');

% Use matlab's cvpartition, it does stratification
if isnumeric(k)
    if k < 1 % Hold-out
        cvs = cvpartition(y, 'HoldOut', k);
    else % K-fold
        cvs = cvpartition(y,'KFold',k);
    end
elseif lower(k)=="loo" % leave-one-out
    if isvector(y)
        nsubjects = numel(y);
    else
        nsubjects = y;
    end
    cvs = cvpartition(nsubjects, 'leaveout');
else
    error('Parameter ''k'' must be either an integer or a char vector / string ''loo''');
end

% Save folds in this cell array
% folds = cell(cvs.NumTestSets,1);

if numel(y) > 1
    if isnumeric(y)
        valid_subjects = sum(~isnan(y));   
    else
        % We're assuming all group labels are correct
        valid_subjects = numel(y);
    end
else
    valid_subjects = y;
end

% We need to account for multiple measurements per subject. All
% measurements per subject must be in the same fold; e.g. 9 measurements 
% (minutes) for meg700 subjects, 5 measurements for meg280 & biofind
for set_idx = 1:cvs.NumTestSets
    train_indices_orig = training(cvs, set_idx);
    test_indices_orig = test(cvs, set_idx);

    train_indices = repmat(train_indices_orig, 1, nmeasurements);
    test_indices = repmat(test_indices_orig, 1, nmeasurements);
    
    train_indices = train_indices(:);
    test_indices = test_indices(:);

    % Just a sanity check to make sure everything's going alright
    if sum(test_indices&train_indices) ~= 0 || ...
        sum(test_indices|train_indices) ~= valid_subjects*nmeasurements
        error('Something''s going wrong');
    end
 
    folds(set_idx) = struct('test_idx', test_indices, 'train_idx', train_indices, ...
        'test_idx_orig', test_indices_orig, 'train_idx_orig', train_indices_orig);
end

% if cvs.NumTestSets == 1
%     folds = folds{1};
% end

end