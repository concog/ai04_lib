function folds = k_folds(y, k, reps)
% K_FOLDS Repeated k-fold cross-validation with stratified folds

% Suppress warnings about missing values
warning('off', 'stats:cvpartition:MissingGroupsRemoved');

rng('shuffle');

folds = cell(reps, 1);

% Repeat as many times as required
for repeat_idx = 1:reps
    folds{repeat_idx} = get_folds(y, k);
end

% Restore warnings
warning('on', 'stats:cvpartition:MissingGroupsRemoved');
end


function folds = get_folds(y, k)

% Use matlab's cvpartition, it does stratification
if isnumeric(k)
    if k < 1 % Hold-out
        cvs = cvpartition(y, 'HoldOut', k);
    else % K-fold
        cvs = cvpartition(y,'KFold',k);
    end
elseif lower(k)=="loo" % leave-one-out
    if isvector(y)
        nsubjects = numel(y);
    else
        nsubjects = y;
    end
    cvs = cvpartition(nsubjects, 'leaveout');
else
    error('Parameter ''k'' must be either an integer or a char vector / string ''loo''');
end


for set_idx = 1:cvs.NumTestSets
    train_indices = training(cvs, set_idx);
    test_indices = test(cvs, set_idx);

    % Just a sanity check to make sure everything's going alright
    if sum(test_indices&train_indices) ~= 0 
        error('Something''s wrong');
    end
 
    folds(set_idx) = struct('test_indices', test_indices, 'train_indices', train_indices);
end

end