function start_parpool(num_workers, mem_per_worker)
%START_PARPOOL Starts a pool of workers in the old CBU cluster

% Need this for cbupool
addpath /hpc-software/matlab/cbu

% Close previous pool
existing_pool = gcp('nocreate');
if (~isempty(existing_pool))
    delete(existing_pool);
end

% TODO: Remove old cluster profiles -- is there a way to do this programmatically?

% Define a Torque cluster; this part is from Darren's dp_matlabpool_start.m
% (in /home/dp01/matlab/lib/)
WallTime = 96;
P = cbupool(num_workers);
P.ResourceTemplate = sprintf('-l nodes=^N^,mem=%dGB,walltime=%d:00:00', num_workers*mem_per_worker, WallTime);
P.JobStorageLocation = '/home/ai04/Workspace/matlab_parallel_jobs/';

% Open the parallel pool
parpool(P, num_workers);

end

