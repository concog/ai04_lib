function char_vec = query_to_char(query)
% Convert cell/string query into two character vectors, one for titles and
% one for filenames
char_vec = struct();

if query{1} == "locations"
    char_vec.filename = char(strjoin(query(4:end), '_'));
    char_vec.title = char(strjoin(query(4:end), '/'));
else
    char_vec.filename = char(strjoin(query, '_'));
    char_vec.title = char(strjoin(query, '/'));
end