function ai04_loadspm

% Modified from /home/dp01/matlab/lib/loadspm

addpath /imaging/local/software/spm_cbu_svn/releases/spm12_fil_r6906
addpath /imaging/local/software/spm_cbu_svn/releases/spm12_fil_r6906/matlabbatch/

% Remove fieldtrips from path; why do we do this?
warning('off', 'MATLAB:rmpath:DirNotFound');
while exist('ft_defaults','file') ~= 0
    rmpath(genpath(fileparts(which('ft_defaults'))))
end
warning('on', 'MATLAB:rmpath:DirNotFound');


%spm('defaults','eeg')

end