function [spectra] = calc_psd(data)
%%calculate PSD

%disp('calculating power spectra')

%spectra=zeros(50,size(data,1),size(data,3));
for m=1:size(data,1)
    for k=1:size(data,3)
        Pxx = pwelch(data(m,:,k)',size(data,2));
          spectra(:,m,k)=Pxx;
    end
end

end