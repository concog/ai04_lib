function [nspectra] = calc_npsd(spectra)
%this function normalizes the spectra prior to the pca step

% normalization step
for k=1:size(spectra,2)
    nspectra(:,k,:)=log(spectra(:,k,:)./repmat(squeeze(mean(spectra(:,k,:),3)),[1 1 size(spectra,3)]));
end
clear k


