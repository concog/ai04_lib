function residuals = regress_timeseries(data, nuisance)
% REGRESS_TIMESERIES Regress nuisance timeseries out of data. Both
% variables must contain timeseries in columns. Assumes there is no
% constant term in 'nuisance'

% Add a constant term
nuisance = [ones(size(nuisance,1),1), nuisance];

% Estimate fit
fit = nuisance*(pinv(nuisance)*data);

% Return residuals
residuals = data - fit;

end