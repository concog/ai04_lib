function values = remove_outliers(values, set_nans)
% REMOVE_OUTLIERS Remove outliers with 1.5*IQR rule
%
%   clean = REMOVE_OUTLIERS(values) removes outliers, i.e. values lower
%   than p25 - 1.5*IQR or higher than p75 + 1.5*IQR
%
%   clean = REMOVE_OUTLIERS(values, set_nans) sets outliers as nans instead
%   of removing them

if nargin < 2
    set_nans = false;
end

prcs = prctile(values, [25, 75]);
iqr = prcs(2) - prcs(1);
outliers = values > prcs(2)+(1.5*iqr) | values < prcs(1)-(1.5*iqr);

if set_nans
    values(outliers) = nan;
else
    values(outliers) = [];
end
end