function [idx, seeds] = kmeans_timeseries(data, k)

if nargin < 2
    k = 5;
end

% Maybe filter the data first?
%data = butter_filter(data, 250, 'bandpass', [0.5, 45]);

% Choose randomly k seeds
init = randsample(size(data, 2), k);
seeds = data(:, init);

previous_i = nan(size(data,2), 1);
iter = 0;
nchanges = 1;

while nchanges > 0 && iter < 20
% Calculate correlations with data and seeds
r = corrcoef([seeds, data]);
seeds_r = r(1:k, 1:k);
data_r = r(k+1:end, 1:k);

[~, idx] = max(data_r, [], 2);

for seed_idx = 1:k
    % Drop seeds if need to
    if sum(idx==seed_idx) == 1
        % Assign to nearest
        [~, seed_i] = sort(seeds_r(seed_idx, :), 'descend');
        idx(idx==seed_idx) = seed_i(2);
        seeds(:, seed_idx) = nan;
    end
end

% Re-calculate seeds
for seed_idx = 1:k    
    seeds(:, seed_idx) = mean(data(:, idx==seed_idx), 2);
end

nchanges = sum(previous_i~=idx);
fprintf('%d changes\n', nchanges);

previous_i = idx;
iter = iter + 1;
end

% Remove nan groups from seeds
nans = isnan(seeds(1,:));
seeds(:, nans) = [];

end