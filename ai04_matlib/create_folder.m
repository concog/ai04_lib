function create_folder( path )
%CREATE_FOLDER Creates a folder if it doesn't already exist
%   CREATE_FOLDER(PATH) checks if folder in path exists, if not, creates it

% Check if this folder exists
if exist(path, 'dir') ~= 7
    % Doesn't exist, create it
    [success, msg] = mkdir(path);
    if ~success
        % Couldn't create the folder
        warning('Couldn''t create folder %s. Error message: %s.', path, msg);
    end
end

end

