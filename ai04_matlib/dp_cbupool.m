function varargout = dp_cbupool(varargin)
% wrapper function to create matlabpool on cbu cluster


[majorv,rem]=strtok(version,'.');
vers=str2double([majorv '.' strtok(rem,'.')]);

[status, hostname]=system('hostname'); %#ok<ASGLU>


% getpref('DCTConfigurations')
% 2009a = 7.8
% 2011a = 7.12
% 2011b = 7.13

% parallel.clusterProfiles available
% 2012a = 7.14
% 2012b = 8.0
% 2013a = 8.1
% 2014a = 8.3
% 2015a = 8.5

if ~isempty(strfind(hostname,'login')) || ~isempty(strfind(hostname,'node'))
    
    if vers>=7.14
        profiles = parallel.clusterProfiles;
        idx=strmatch('CBU_Cluster_Import',profiles); %#ok<MATCH2>
        if isempty(idx)
            P=parcluster(parallel.importProfile('/hpc-software/matlab/cbu/CBU_Cluster.settings'));
        else
            msg=sprintf('\nNB - profile CBU_Cluster_Import already exists.\nYou may want to check you are using the most recent version of the profile\n');
            disp(msg)
            P=parcluster('CBU_Cluster_Import');
        end
        
        if nargin>0
            P.NumWorkers=varargin{1};
        end
        
        P.ResourceTemplate='-l nodes=^N^,mem=4GB,walltime=24:00:00';
        
        if nargout >0
            varargout{1} = P;
            return
        else
            if vers>=8.3
                parpool(P,P.NumWorkers);
            else
                matlabpool(P);
            end
        end
        
        %parallel.defaultClusterProfile
    else
        msg=sprintf('\ncbupool is currently only supported in Matlab versions 2012a and above.\nPlease use a more recent version of Matlab.\n');
        disp(msg);
    end
else
    msg=sprintf('\ncbupool is currently only supported on the new cluster.\nPlease open a session on one of the login nodes.\n');
    disp(msg);
    
end
