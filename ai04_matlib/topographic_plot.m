function [h1,h2] = topographic_plot(values, merge)

if size(values,1) == 1
    values = values';
end

% Note: SPM needs to be in path
if exist('spm_eeg_plotScalpData', 'file') ~= 2
    addpath /home/dp01/matlab/lib
    loadspm 12
    rmpath /home/dp01/matlab/lib
end

% Load channel information
info = load('planar_channel_topoplot_info.mat');

if (exist('merge', 'var'))
    switch merge
        case 'separate'
            coordsx = info.coords2(:,1:2:end);
            coordsy = info.coords2(:,2:2:end);
            [z1,h1] = spm_eeg_plotScalpData(values(1:2:end), coordsx, info.labels(1:2:end));
            [z2,h2] = spm_eeg_plotScalpData(values(2:2:end), coordsy, info.labels(2:2:end));
            
        case 'odd'
            [z1,h1] = spm_eeg_plotScalpData(values(1:2:end), info.coords2(:,1:2:end), info.labels(1:2:end));
            
        case 'even'
            [z1,h1] = spm_eeg_plotScalpData(values, info.coords2(:,2:2:end), info.labels(2:2:end));
            
        case 'all'
            coordsx = info.coords2(:,1:2:end);
            coordsy = info.coords2(:,2:2:end);
            spm_eeg_plotScalpData(values(1:2:end), coordsx, info.labels(1:2:end));
            spm_eeg_plotScalpData(values(2:2:end), coordsy, info.labels(2:2:end));
            spm_eeg_plotScalpData(values, info.coords2, info.labels);
    
            % Now create destination graph
            figure(4)
            set(gcf, 'Position', [100, 100, 600, 1000])
            ax = zeros(3,1);
            for i = 1:3
                ax(i)=subplot(3,1,i);
            end
            % Now copy contents of each figure over to destination figure
            % Modify position of each axes as it is transferred
            for i = 1:3
                figure(i)
                h = get(gcf,'Children');
                newh = copyobj(h,4);
                for j = 1:length(newh)
                    posnewh = get(newh(j), 'Position');
                    possub  = get(ax(i), 'Position');
                    set(newh(j), 'Position', [posnewh(1) possub(2) posnewh(3) possub(4)])
                end
                if i == 1
                    newh(4).Title.String = 'Odd channels';
                elseif i == 2
                    newh(4).Title.String = 'Even channels';
                else
                    newh(4).Title.String = 'Average of odd and even channels';
                end
                delete(ax(i));
            end
            close 1 2 3
        
        otherwise
            msg = ['Parameter value ' merge ' not defined'];
            error(msg);
    end
else
    [z1,h1] = spm_eeg_plotScalpData(values, info.coords2, info.labels);
end

end