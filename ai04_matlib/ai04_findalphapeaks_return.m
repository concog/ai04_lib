function Out = ai04_findalphapeaks_return(data, fs, showplots)

% Original code by Darren Price @ /imaging/dp01/toolboxes/alphapeaks

% % data should be ncomponents x nsamples x ntrials 
% ncomponents = size(data,1);
% nsamples = size(data,2);
% ntrials = size(data,3);
% 
% % Fmat = matrix of power spectra calculated using welch's method
% Fmat = zeros(ncomponents,nsamples);
% for component = 1:ncomponents
%     F = zeros(1,nsamples);
%     for trial_idx = 1:ntrials
%         F = F+abs(fft(data(component,:,trial_idx))).^2;
%     end
%     Fmat(component,:) = F;
% end
% 
% % Get the frequencies
% freqs = fs*(0:(nsamples/2))/nsamples;

% Out.Fmat = Fmat;

[pxx, f] = pwelch(data, [], [], [], fs);

% for chi = 1:ncomponents
    
%     st = 1; % clip left points from spectrum
    
    % Create F
%     F = Fmat(chi,st+1:floor(nsamples/2)+1);
%     f = freqs(st+1:floor(nsamples/2)+1);
    
    % interporlate on to new grid - sub gamma range
    % search freqs which index corresponds to 45Hz
%     flim = 45;
%     x = linspace(st,499,500-st);
%     xi = linspace(st,flim,500-st);
%     xi = linspace(freqs(st+1), flim, length(f));
%     Finterp = interp1(f,F,xi);
    
    [~, idx] = closest(f,40);
    xi = f(1:idx);
    Finterp = movmean(pxx(1:idx), 25);
    
    % take mean power
%     delta = f>=1 & f<= 3;
%     theta = f>=4 & f<=8;
%     DeltaPower = mean(Fmat(delta).^0.5);
%     ThetaPower = mean(Fmat(theta).^0.5);
    
    % Log transform and fit log(exp(x)) to chosen baseline points
%     Flog = log(Finterp);
%     [fl1v,fl1] = closest(xi,5);
%     [fl2v,fl2] = closest(xi,40);
%     
%     % find the slope of log(exp(x)) and create 1/f distribution fitting F
%     Fslope = (Flog(fl2)-Flog(fl1))/(fl2v-fl1v);
%     Fx = xi*Fslope;
%     Fi = Flog(fl1)-Fx(fl1); %#ok<*AGROW> % intercept at point fl1
%     Fdistx = exp(Fx+Fi);
    % plot(xi,Fdistx,'r',xi,Finterp,'b',xi,Finterp-Fdistx,'m')
    
    % Fit a 1/f curve
    idx = ~isnan(Finterp);
    lin_fit = fit(xi(idx), log(Finterp(idx)), 'poly1');
    Fdistx = exp(xi*lin_fit.p1 + lin_fit.p2);
    
    % Don't use correction if goodness of fit isn't good enough or if flag
    % indiates bad fit
%     if gof.adjrsquare < 0.9 || output.exitflag < 1
%         Fsub = Finterp;
%     else
        % create 1/f corrected spectra
    Fsub = Finterp-Fdistx;
    
%     end
    
%     [~, x8] = closest(xi, 8);
%     [~, x12] = closest(xi, 12);
%     Fsub = detrend(Finterp);
%     Fsub = Fsub - min(Fsub(x8:x12));

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if showplots
        % Plot frequency fit for presentation.
        figure()
        set(gcf,'color','white','position',[897   795   615   218])
%         freq = linspace(4,45,length(Finterp));
        plot(xi,Finterp,'b')
        xlabel('Frequency')
        ylabel('Power')
        hold on
        plot(xi,Fdistx,'m')
        hold off
        figure()
        set(gcf,'color','white','position',[897   795   615   218])
        plot(xi,Fsub,'b')
        xlabel('Frequency')
        ylabel('Power')
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
%     [~,x0i] = dp_closest(xi,10);
%     initvals = [10 1]; % set initial values
%     
%     % scale the gaussian for initial value esitmation
%     g1 = normpdf(initvals(1),initvals(1),initvals(2))*Finterp(x0i);
%     
%     % Set parameters of fitting and fit data
%     P.Inc = [1 0.5 g1/4];
%     P.initvals = [initvals g1]; % reset init vals using new amplitude
%     P.FitWin = [5 13];
%     P.showplots = showplots;
%     P.x = xi;
%     P.decelfactor = 2;
%     P.accelfactor = 1.2;
%     P.cflim = 1e-6;
    
    % Let's try a couple of different methods
%     O = spectral_fitting(Fsub,P);
%     O = spectral_fitting(Finterp, P);

    % Fit a few models with different starting values, choose the best one
    start_points = [8, 9, 10, 11, 12];
    mdls = cell(1, length(start_points));
    adjRsquared = zeros(1, length(start_points));
    % Use only range 5-15Hz
    [~, x8] = closest(xi, 8);
    [~, x12] = closest(xi,12);
    
    % Estimate baseline by taking median of bottom 10% of histogram => 5th
    % percentile
%     Fsub = Fsub - prctile(Fsub(x8:x12), 5);
    
    
    for idx = 1:length(start_points)
            
        mdl = fit(xi, Fsub, 'gauss1', 'Lower', [0, 7, 0.05], 'Upper', [Inf, 13, 3], 'StartPoint', [max(Fsub(x8:x12)), start_points(idx), 0.5]);
%         [mdl, gof] = fit(xi', Finterp', 'gauss1', 'Lower', [0, 7, 0.01], 'Upper', [Inf, 13, 3], 'StartPoint', [max(Finterp), start_points(idx), 0.5]);
        mdls{idx} = mdl;
        
        % Evaluate the model at points in xi
        xp = feval(mdl,xi);

        % Calculate the range of peak (mean +- 3*std)
        low = mdl.b1 - 2*mdl.c1;
        [~, low_idx] = closest(xi, low);
        hi = mdl.b1 + 2*mdl.c1;
        [~, hi_idx] = closest(xi, hi);

        % Estimate the fit of the peak
        fit_mdl = fitlm(Fsub(low_idx:hi_idx), xp(low_idx:hi_idx));
        
        % Save adjusted R squared for model comparison
        % R squared can be relatively low for a good fit if the residuals 
        % have some structure (i.e. aren't gaussian distributed)
        adjRsquared(idx) = fit_mdl.Rsquared.Adjusted;
    
        % Check confidence intervals of the estimates. If CI of peak's
        % height contains zero, it implies poor fit => reduce R squared
        ci = confint(mdl);
        if ci(1,1) <= 0
            adjRsquared(idx) = 0;
        end
        % Punish for large confidence interval
%         adjRsquared(idx) = adjRsquared(idx) - (ci(2,1)-ci(1,1))/max(Fsub(x8:x12));
        % Reward higher peaks
%         height = 1 - (mdl.a1/max(Fsub(x8:x12)));
%         adjRsquared(idx) = mean([adjRsquared(idx), (mdl.a1/max(Fsub(x8:x12)))]);
%         adjRsquared(idx) = adjRsquared(idx) - (ci(2,2) - ci(1,2));

    end
     
    % Choose the model with highest adjusted R squared
    [~, idx] = max(adjRsquared);
    mdl = mdls{idx};
           
    if showplots
        % Evaluate the model at points in xi (for plotting)
        xp = feval(mdl,xi);

        figure();
    %     plot(xi,Fsub), hold on, plot(xi, xp); plot([O.cv(1) O.cv(1)], [0, max(Fsub)]);
        plot(xi,Fsub),hold on, plot(xi, xp); 
        if exist('O', 'var')
            plot([O.cv(1) O.cv(1)], [0, max(Finterp)]);
        end
        title_msg = ['adjRsquared: ' num2str(adjRsquared(idx)) ', b1: ', num2str(mdl.b1)];
        title(title_msg);
        
    end
    
    Out.location = mdl.b1;
    Out.confidence = adjRsquared(idx);
    %Out.alpha(chi,:) = O.cv(1);
%     Out.psd = Fsub;
% end
end

