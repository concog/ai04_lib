function [clval, clind] = closest(x, val)

clval = zeros(size(val));
clind = clval;

% Find closest value to val and index in x
for ii = 1:numel(val)
    ind = find(abs(x-val(ii)) == min(abs(x-val(ii))));
    if length(ind) == 2
        clind(ii) = ind(2);
    else
        clind(ii) = ind;
    end
    
    clval(ii) = x(clind(ii));
end

end