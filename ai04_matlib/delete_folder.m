function delete_folder(path)
% Check if this folder exists, and if it does, delete it

if isdir(path)
    % Make sure all files are closed
    fclose('all');
    
    % Try to remove the folder 
    if ~rmdir(path, 's')
        error_msg = ['Couldn''t remove folder ' path ', aborting'];
        error(error_msg);
    end
end
   
end