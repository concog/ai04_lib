function log_msg(silent, msg)
if silent
    return;
end

timestamp = datestr(now, 'yyyy-mm-ddTHH:MM:SSZ');
caller = dbstack;

fprintf('[%s] %s (%s: line %d)\n', timestamp, msg, caller(2).file, caller(2).line);

end